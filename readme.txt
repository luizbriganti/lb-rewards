=== Plugin Name ===

Contributors: Luigi Briganti
Plugin Name: LB Rewards for WooCommerce
Plugin URI: http://www.linebreak.it/
Tags: wp, woocommerce, rewards, linebreak
Author URI: http://www.linebreak.it/
Author: Linebreak
Donate link: https://paypal.me/luizbriganti
Requires at least: 4.3
Tested up to: 5.1
Stable tag: 0.1a
Version: 0.1a

== Description ==

This plugin allows you to manage rewards plans for users. With this plugin users may load a receipt for their shopping offline and request a conversion in points. The shop manager can generate a coupon from the points collected by the user.

== Installation ==

If not installing it directly from the Wordpress Plugin Repository, in your website wp-admin go to Plugins->Add new
and upload the zip folder of the plugin and wait for the installation to be completed. After that, if no errors occurred,
activate plugin and then you are ready to go.

== Upgrade Notice ==

== Screenshoots ==

== Donations ==

If you want to support us, you can donate to paypal.me/luizbriganti. Thanks, in advance!
