<?php
function lb_reward_request_form(){
  global $current_user;
  wp_get_current_user();
  $points = get_user_meta($current_user->ID, '_lb_points', true);
  echo '<h2>'. sprintf(__('%s Rewards', 'lb-rewards'), get_bloginfo('name')) . '</h2>';
  if($points){
    echo '<p class="reward-message">'. sprintf( __('Your balance is of <strong>%u points</strong>. You may ask to convert your points into coupon.', 'lb-rewards') , $points ) .'</p>';
  } else {
    echo '<p class="reward-message">'. __('You have no points yet', 'lb-rewards') .'</p>';
  }

  $args = array('post_type' => 'shop_coupon', 'posts_per_page' => -1, 'meta_key' => 'user_ID', 'meta_value' => $current_user->ID, 'orderby' => 'date', 'order' => DESC);
  $loop = new WP_Query($args);
  if($loop->have_posts()) :
    echo sprintf( __('You have %u unspent coupon:', 'lb-rewards'), count($loop) );
    echo '<ul class="coupons">';

    while($loop->have_posts()) : $loop->the_post();

      $amount = get_post_meta(get_the_ID(), 'coupon_amount', true);

      echo '<li class="coupon"><span>'. get_the_title() .'<span> - <span>'. sprintf( __('Value: %u&percnt; discount', 'lb-rewards'), $amount) .'</span></li>';

    endwhile;

    echo '</ul>';

  endif;

?>

  <form class="send-request" method="post" action="" enctype="multipart/form-data">
    <h3><?php _e('Submit a request for reward credit', 'lb-rewards'); ?></h3>
    <p class="form-row form-row-wide">
      <?php _e('Have you bought one of our products in a store offline? You may now collect points from your shopping. Just upload a photo of your receipt and the amount spent will be converted in points.', 'lb-rewards'); ?>
    </p>
    <p class="form-row form-row-wide">
      <label for="lb-rewards-bill"><?php _e('Upload a photo of your receipt', 'lb-rewards'); ?></label>
      <input type="file" name="lb_bill" id="lb-rewards-bill" required />
    </p>
    <p class="form-row form-row-wide">
      <label for="lb-rewards-note"><?php _e('Insert a note for your request.', 'lb-rewards'); ?></label>
      <textarea name="lb_note" id="lb-rewards-note" required></textarea>
    </p>
    <p class="form-row form-row-wide">
      <?php
      $t = time();
      $now = date('dmY-H:i', $t);
      ?>
      <input type="hidden" name="lb_rewards_user" value="<?php echo $current_user->ID; ?>" id="lb-rewards-user" />
      <input type="hidden" name="lb_rewards_title" value="<?php echo $current_user->user_login .'_'. $now ; ?>" id="lb-rewards-title" />
      <input type="hidden" name="lb_rewards_nonce" value="<?php echo wp_create_nonce('lb_rewards_nonce'); ?>" id="lb-rewards-nonce" />
      <input type="submit" class="button btn" name="lb_submit_request" value="<?php _e('Submit request', 'lb-rewards'); ?>" id="lb-submit-request" />
    </p>
  </form>

  <?php

}
