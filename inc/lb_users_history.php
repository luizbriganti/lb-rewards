<?php

function lb_users_history(){

  echo '<h1>'. __('User\'s transactions history', 'lb-rewards') .'</h1>';
  echo '<p class="reward-message">'. __('Here you can keep trace of the transactions for each user.', 'lb-rewards') .'</p>';
  echo '<ul class="users">';
  echo '<li class="user first-row"><span>'. __('Users', 'lb-rewards') .'</span><span>'. __('Total transactions', 'lb-rewards') .'</span><span>'. __('Details', 'lb-rewards') .'</span>';

  $args = array('orderby' => 'login', 'order' => 'ASC');
  $users = get_users($args);

  // ini_set('memory_limit', '1024M');

  // global $wpdb;
  // $users = $wpdb->get_results('SELECT * FROM lb2k18_users WHERE user_status = 0');
  // var_dump($users);

  foreach($users as $user){
    $transactions = get_user_meta($user->ID, '_lb_transactions', true);
    $pts = get_user_meta($user->ID, '_lb_points', true);
    if($transactions){
      $count = count($transactions);
      $viewall = '<span class="view">'. __('View all', 'lb-rewards') .'</span>';
    } else {
      $count = 0;
      $viewall = '<span>'. __('No transactions to show', 'lb-rewards') .'</span>';
    }

    echo '<li class="user"><span>'. $user->user_login .'</span><span>'. $count .'</span>'. $viewall;
    echo '<div class="transactions-container">
          <ul class="transactions"><li class="transaction">'. __('Date', 'lb-rewards') .'</li><li class="transaction">'. __('Points gained/spent', 'lb-rewards') .'</li><li class="transaction">'. __('Coupon generated', 'lb-rewards') .'</li><li class="transaction">'. __('Notes', 'lb-rewards') .'</li></ul>';
          $points = 0;
          $coupons = 0;
    foreach($transactions as $transaction){
      $substr = substr($transaction['totalpoints'], 0, 1);
      if( $substr == '-'){
        $class = 'red';
      } else {
        $class = 'green';
      }

      if($transaction['coupon_generated'] == 'Yes'){
        $abs = abs($transaction['totalpoints']);
        $cou = $transaction['coupon_from_receipt_amount'];
        $tot = ($abs + $cou)/10;
        $note = sprintf(__('This coupon was generated subtracting %1u points from user\'s balance and %2u points from receipt uploaded, for a total of %3u&percnt; discount.', 'lb-rewards'), $abs, $cou, $tot);
      } else {
        $note = '';
      }

      echo '<ul class="transactions '. $class .'">';
      echo '<li class="transaction">'. $transaction['date'] .'</li>';
      echo '<li class="transaction">'; if($substr != '-'){ echo '+'; } echo $transaction['totalpoints'] .'</li>';
      echo '<li class="transaction">'; if($transaction['coupon_generated'] == 'Yes' || $transaction['coupon_generated'] == 'Sì'){ echo __('Yes', 'lb-rewards'); } else { echo __('No', 'lb-rewards'); } echo '</li>';
      echo '<li class="transaction">'. $note .'</li>';
      echo '</ul>';

      $points += $transaction['totalpoints'];
      $coupons = $transaction['coupon_generated'] == 'Yes' || $transaction['coupon_generated'] == 'Sì' ? count($transaction['coupon_generated']) : '0';
    }
    echo '<ul class="transactions"><li class="transaction">'. __('User\'s current balance: ', 'lb-rewards') . '<strong>'. $pts .'</strong></li><li class="transaction">'. __('Transactions balance: ', 'lb-rewards') . '<strong>'. $points .'</strong></li><li class="transaction">'. __('Coupons generated: ', 'lb-rewards') . '<strong>'. $coupons .'</strong></li><li class="transaction"></li></ul>
    </div>';
    echo '</li>';
  }
  echo '</ul>';

}
