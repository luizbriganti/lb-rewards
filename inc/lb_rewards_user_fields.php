<?php

add_action( 'show_user_profile', 'lb_rewards_user_fields' );
add_action( 'edit_user_profile', 'lb_rewards_user_fields' );

function lb_rewards_user_fields( $user ) { ?>
    <h3><?php _e("Rewards information", "lb-rewards"); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="lb_points"><?php _e("User balance", 'lb-rewards'); ?></label></th>
        <td>
            <input type="text" name="lb_points" id="address" value="<?php echo esc_attr( get_the_author_meta( '_lb_points', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Update user balance here", "lb-rewards"); ?></span>
        </td>
    </tr>
    </table>
<?php }

add_action( 'personal_options_update', 'save_rewards_user_fields' );
add_action( 'edit_user_profile_update', 'save_rewards_user_fields' );

function save_rewards_user_fields( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) {
        return false;
    }
    update_user_meta( $user_id, '_lb_points', $_POST['lb_points'] );
}
