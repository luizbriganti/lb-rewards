<?php
global $reward_boxes;
$reward_boxes = array();

// Categories

$reward_boxes[] = array(
    'id' => 'lb_reward_users',
    'class' => array('row', 'half'),
    'title' => __('User', 'lb-rewards'),
    'pages' => array('reward', 'link'), // multiple post types
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
          'name' => '',
          'desc' => '',
          'id' => 'lb_reward_user',
          'type' => 'text',
          'std' => ''
        ),
    )
);

$reward_boxes[] = array(
    'id' => 'lb_reward_points',
    'class' => array('row', 'half'),
    'title' => __('Points to credit/debit', 'lb-rewards'),
    'pages' => array('reward', 'link'), // multiple post types
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
          'name' => '',
          'desc' => __('If adding a negative value, points will be subtracted from user\'s balance.', 'lb-rewards'),
          'id' => 'lb_reward_point',
          'type' => 'number',
          'std' => ''
        ),
    )
);

$reward_boxes[] = array(
    'id' => 'lb_reward_notes',
    'class' => array('row', 'half'),
    'title' => __('A note from user', 'lb-rewards'),
    'pages' => array('reward', 'link'), // multiple post types
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
          'name' => '',
          'desc' => '',
          'id' => 'lb_reward_note',
          'type' => 'user_notes',
          'std' => ''
        ),
    )
);

$reward_boxes[] = array(
    'id' => 'lb_reward_coupons',
    'class' => array('row', 'half'),
    'title' => __('Generate a coupon?', 'lb-rewards'),
    'pages' => array('reward', 'link'), // multiple post types
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
          'name' => '',
          'desc' => __('Insert a value to generate one coupon from the given import (10 points: 1% discount)', 'lb-rewards'),
          'id' => 'lb_reward_coupon',
          'type' => 'number',
          'std' => ''
        ),
    )
);



foreach ($reward_boxes as $meta_box) {
    $my_box = new Rewards_meta_box($meta_box);
}

class Rewards_meta_box {

    protected $_meta_box;

    // create meta box based on given data
    function __construct($meta_box) {
        $this->_meta_box = $meta_box;
        add_action('admin_menu', array(&$this, 'add'));

        add_action('save_post', array(&$this, 'save'));
    }

    /// Add meta box for multiple post types
    function add() {
        foreach ($this->_meta_box['pages'] as $page) {
            add_meta_box($this->_meta_box['id'], $this->_meta_box['title'], array(&$this, 'show'), $page, $this->_meta_box['context'], $this->_meta_box['priority']);
        }
    }

    // Callback function to show fields in meta box
    function show() {
        global $post;

        // Use nonce for verification
        echo '<input type="hidden" name="lb_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

        echo '<div>';
        foreach ($this->_meta_box['fields'] as $field) {
            // get current post meta data
            echo '<div class="row half">';
            $meta = get_post_meta($post->ID, $field['id'], true);
            $user = get_user_by('ID', $meta);
            $balance = get_user_meta($user->ID, '_lb_points', true);
            echo '<label for="', $field['id'], '">', $field['name'], '</label>';
            switch ($field['type']) {
                case 'text':
                    echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $user->user_login : $field['std'], '" ', $field['id'] == 'lb_reward_user' ? 'disabled' . ' style="border: none; color: black;box-shadow: initial;"' : ''  ,' />';
                    if($field['id'] == 'lb_reward_user'){
                    echo  sprintf(__('This user has a balance of %u points', 'lb-rewards'), $balance);
                    } else {
                      $field['desc'];
                    }
                    break;
                case 'number':
                    echo '<input type="number" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" max="10000"/>', $field['desc'];
                    if($field['id'] == 'lb_reward_coupon'){
                      echo '<br /><input type="checkbox" name="subtract_points" id="subtract_points" />';
                      echo '<label for="subtract_points">'. __('If checked, you add to the coupon the amount subtracted from the user\'s balance specified in the "Point to credit/debit" box.', 'lb-rewards') .'</label>';
                    }
                    break;
                case 'user_notes':
                    echo '<p id="', $field['id'], '">', $meta ? $meta : $field['std'], '</p>';
                    break;
                case 'textarea':
                    echo '<textarea name="', $field['id'], '" id="', $field['id'], '">', $meta ? $meta : $field['std'], '</textarea>', $field['desc'];
                    break;
            }

            echo '</div>';
        }

        echo '</div>';
    }

    // Save data from meta box
    function save($post_id) {
        // verify nonce
        if (!wp_verify_nonce($_POST['lb_meta_box_nonce'], basename(__FILE__))) {
            return $post_id;
        }

        // check autosave
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return $post_id;
        }

        // check permissions
        if ('reward'  == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id)) {
                return $post_id;
            }
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }

        foreach ($this->_meta_box['fields'] as $field) {
          $old = get_post_meta($post_id, $field['id'], true);
          $new = $_POST[$field['id']];

          if($field['id'] == 'lb_reward_user'){
            if($new == '' || !$new || $new == $old || $new != $old){
              update_post_meta($post_id, $field['id'], $old);
            }
          }
          if($field['id'] == 'lb_reward_coupon'){
            if($new && $new != $old){
              update_post_meta($post_id, $field['id'], $new);
            }
          }

          if($field['id'] == 'lb_reward_point'){
            $userID = get_post_meta($post_id, 'lb_reward_user', true);
            $oldpoints = get_user_meta($userID, '_lb_points', true);
            $allTrans = get_user_meta($userID, '_lb_transactions', true);
            if($new && $new != $old){
              if(!$oldpoints){
                add_user_meta($userID, '_lb_points', $new);
              } elseif($oldpoints){
                $newpoints = $new + $oldpoints;
                if($newpoints > 0){
                  update_user_meta($userID, '_lb_points', $newpoints, $oldpoints);
                } elseif ($newpoints < 0){
                  update_user_meta($userID, '_lb_points', '0', $oldpoints);
                }
              }

              if(!empty($_POST['lb_reward_coupon'])){
                $check = __('Yes', 'lb-rewards');
                $receipt = $_POST['lb_reward_coupon'];
              } else {
                $check = __('No', 'lb-rewards');
                $receipt = '';
              }

              if(!$allTrans){
                $transactions[] = array('date' => date('d-m-Y, H:i'), 'totalpoints' => $new, 'coupon_generated' => $check, 'coupon_from_receipt_amount' => $receipt);
                add_user_meta($userID, '_lb_transactions', $transactions);
              } elseif($allTrans){
                $allTrans[] = array('date' => date('d-m-Y, H:i'), 'totalpoints' => $new, 'coupon_generated' => $check, 'coupon_from_receipt_amount' => $receipt);
                update_user_meta($userID, '_lb_transactions', $allTrans);
              }

              update_post_meta($post_id, $field['id'], $new);

            }
          }
        }
    }
}

add_action('save_post', 'lb_create_coupon', 1, 1);

function lb_create_coupon($post_ID){

  $uID = get_post_meta($post_ID, 'lb_reward_user', true);
  $userID = get_user_by('ID', $uID);
  $couponTitle = $userID->user_login.'-'. date('dmY');
  if(get_post_type($post_ID) == 'reward'){
    if(!empty($_POST['lb_reward_coupon'])){
      if(!isset($_POST['subtract_points'])){
        $new = $_POST['lb_reward_coupon'];
      } elseif(isset($_POST['subtract_points'])){
        $new = $_POST['lb_reward_coupon'] + abs($_POST['lb_reward_point']);
      }
      $coupon = array(
        'post_title' => $couponTitle,
        'post_type' => 'shop_coupon',
        'post_status' => 'publish',
      );

      $couponID = wp_insert_post($coupon);

      $amount = $new / 10;
      $month = time() + (30 * 86400);
      $expiry = date('d-m-Y', $month);

      error_log($amount, $expiry);
      update_post_meta($couponID, 'coupon_amount', $amount);
      update_post_meta($couponID, 'individual_use', 'yes');
      update_post_meta($couponID, 'exclude_sale_items', 'yes');
      update_post_meta($couponID, 'expiry_date', $expiry);
      update_post_meta($couponID, 'usage_limit', '1');
      update_post_meta($couponID, 'usage_limit_per_user', '1');
      update_post_meta($couponID, 'user_ID', $uID);
      update_post_meta($couponID, 'customer_email', serialize($userID->user_email));
    }
  }
}

add_filter( 'gettext', 'lb_rewards_change_publish_button', 10, 2 );

function lb_rewards_change_publish_button( $translation, $text ) {

if(get_post_type() == 'reward'){

  if ( $text == 'Publish' ){
      return __('Accept request', 'lb-rewards');
    }
  if ( $text == 'Update' ){
      return __('Repeat transaction', 'lb-rewards');
  }

}

return $translation;
}
