<?php

function lb_insert_reward_request(){

  if(isset($_POST['lb_submit_request'])){

    $data = $_POST;
    $nonce = $data['lb_rewards_nonce'];

    if(wp_verify_nonce($nonce, 'lb_rewards_nonce')){

      $title = $_POST['lb_rewards_title'];
      $userID = $_POST['lb_rewards_user'];
      $note = $_POST['lb_note'];

      if($_FILES['lb_bill']['name'] != ''){
        $reward = array(
          'post_title' => $title,
          'post_type' => 'reward',
          'post_description' => $note,
          'post_author' => $userID,
        );
      }

      $reward_id = wp_insert_post($reward, $wp_error = 'Error');

      if($reward_id){
        if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );

        $uploadedfile = $_FILES['lb_bill'];
        $upload_overrides = array( 'test_form' => false );
        if( $_FILES['lb_bill']['name'] != '' ){
          $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
          update_post_meta($reward_id, '_lb_rewards_bill', $movefile['url'] );

            $image_url        = $movefile['url'] ; // Define the image URL here
            $image_name       = basename($image_url);
            $upload_dir       = wp_upload_dir(); // Set upload folder
            $image_data       = file_get_contents($image_url); // Get image data
            $unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
            $filename         = basename( $unique_file_name ); // Create image file name

            // Check folder permission and define file location
            if( wp_mkdir_p( $upload_dir['path'] ) ) {
                $file = $upload_dir['path'] . '/' . $filename;
            } else {
                $file = $upload_dir['basedir'] . '/' . $filename;
            }

            // Create the image  file on the server
            file_put_contents( $file, $image_data );

            // Check image file type
            $wp_filetype = wp_check_filetype( $filename, null );

            // Set attachment data
            $attachment = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_title'     => sanitize_file_name( $filename ),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

            // Create the attachment
            $attach_id = wp_insert_attachment( $attachment, $file, $reward_id );

            // Include image.php
            require_once(ABSPATH . 'wp-admin/includes/image.php');

            // Define attachment metadata
            $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

            // Assign metadata to attachment
            wp_update_attachment_metadata( $attach_id, $attach_data );

            // And finally assign featured image to post
            set_post_thumbnail( $reward_id, $attach_id );
        } else {
          error_log(__('No images found', 'lb-rewards'));
        }

        update_post_meta($reward_id, 'lb_reward_user', $userID);
        update_post_meta($reward_id, 'lb_reward_note', $note);

        echo '<p class="reward-message">'. __('Your request has been sent correctly', 'lb-rewards') .'</p>';
        echo lb_reward_request_form();
      }
    } else {
      echo '<p class="reward-message">'. __('Something went wrong. Please retry or contact the administrator') .'</p>';
      lb_reward_request_form();
    }
  } else {
    lb_reward_request_form();
  }

}
