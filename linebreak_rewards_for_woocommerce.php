<?php
/*
Plugin Name: LB Rewards for WooCommerce
Description: Manage rewards plans for users. With this plugin users may load a receipt for their shopping offline and request a conversion in points. The shop manager can generate a coupon from the points collected by the user.
Version: 0.1a
Author: Luigi Briganti
Author URI: http://www.linebreak.it
Text Domain: lb-rewards
*/

defined( 'ABSPATH' ) or die( 'Ritenta la prossima volta!' );
define( 'lb_rw_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

add_action( 'init', 'lb_rw_txt_dmn');

function lb_rw_txt_dmn(){
  load_plugin_textdomain( 'lb-rewards', false, dirname( plugin_basename( __FILE__ ) ) );
}

function lb_rewards_endpoint() {
  add_rewrite_endpoint( 'rewards', EP_ROOT | EP_PAGES );
}

function lb_rewards_admin_scripts(){
  wp_enqueue_script('reward_admin_js', lb_rw_PLUGIN_URL .'/assets/js/admin.js' );
  wp_enqueue_style('materialdesignicons', lb_rw_PLUGIN_URL .'/assets/css/materialdesign/css/materialdesignicons.min.css' );
  wp_enqueue_style('reward_admin_css', lb_rw_PLUGIN_URL .'/assets/css/admin.css' );
}

function lb_rewards_frontend_scripts(){
  wp_enqueue_style('materialdesignicons', lb_rw_PLUGIN_URL .'/assets/css/materialdesign/css/materialdesignicons.min.css' );
  wp_enqueue_style('reward_frontend_css', lb_rw_PLUGIN_URL. '/assets/css/frontend.css');
}

add_action('admin_enqueue_scripts', 'lb_rewards_admin_scripts');
add_action('wp_enqueue_scripts', 'lb_rewards_frontend_scripts');

/* Includes */

include( plugin_dir_path( __FILE__ ) . 'inc/lb_rewards_cpt.php');
include( plugin_dir_path( __FILE__ ) . 'inc/lb_reward_endpoint.php');
include( plugin_dir_path( __FILE__ ) . 'inc/lb_reward_request_form.php');
include( plugin_dir_path( __FILE__ ) . 'inc/lb_insert_reward_request.php');
include( plugin_dir_path( __FILE__ ) . 'inc/lb_rewards_meta.php');
include( plugin_dir_path( __FILE__ ) . 'inc/lb_rewards_user_fields.php');
include( plugin_dir_path( __FILE__ ) . 'inc/lb_users_history.php');
include( plugin_dir_path( __FILE__ ) . 'inc/lb_rewards_admin_columns.php');

add_action( 'init', 'lb_rewards_endpoint' );

add_filter( 'woocommerce_account_menu_items', 'lb_rewards_items' );

function lb_rewards_items( $items ) {

  $items[ 'rewards' ] = __('Rewards', 'lb-rewards');

  return $items;

}

add_action( 'woocommerce_account_rewards_endpoint', 'lb_rewards_content' );

function lb_rewards_content(){

  reward_endpoint();

}

add_action('do_meta_boxes', 'lb_rewards_replace_featured_image_box');
function lb_rewards_replace_featured_image_box()
{
  remove_meta_box( 'postimagediv', 'reward', 'side' );
  add_meta_box('postimagediv', __('Receipt Photo'), 'post_thumbnail_meta_box', 'reward', 'normal', 'high');
}

function lb_rewards_setup_menu(){

  $title = __('User transactions history', 'lb-rewards');

  add_submenu_page('edit.php?post_type=reward', $title, $title, 'manage_options', 'lb-users-history', 'lb_users_history');
}

add_action('admin_menu', 'lb_rewards_setup_menu');
