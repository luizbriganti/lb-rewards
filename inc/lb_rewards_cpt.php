<?php

add_action('init', 'lb_rewards_cpt');

	function lb_rewards_cpt() {

		$types = array(

		// Rewards
			array('the_type' => 'reward',
						'single' => sprintf(__('%s Reward', 'lb-rewards'), get_bloginfo('name')),
						'plural' => sprintf(__('%s Rewards', 'lb-rewards'), get_bloginfo('name')),
						'exclude-from-search' => 'true',
						'menu-position' => 21,
						'menu-icon'=> 'dashicons-awards',
						'taxonomy' => array(''),
						'support' => array('title', 'thumbnail','author'),
						'capability_type'     => 'post',
						'rewrite' => 'reward'
					),
		);

		foreach ($types as $type) {

				  $the_type = $type['the_type'];
				  $single = $type['single'];
				  $plural = $type['plural'];
					$exclude = $type['exclude-from-search'];
					$position = $type['menu-position'];
					$menu_icon = $type['menu-icon'];
					$taxonomies = $type['taxonomy'];
					$support = $type['support'];
					$capability_type = $type['capability_type'];
					$rewrite = $type['rewrite'];

				$labels = array(
				    'name' => _x($plural, 'post type general name', 'lb-rewards'),
				    'singular_name' => _x($single, 'post type singular name', 'lb-rewards'),
				    'add_new' => __('Add new', 'lb-rewards'),
				    'add_new_item' => __('Add new', 'lb-rewards'),
				    'edit_item' => sprintf(__('Edit %s', 'lb-rewards'), $single),
				    'new_item' => sprintf(__('New %s', 'lb-rewards'), $single),
				    'view_item' => sprintf(__('View %s', 'lb-rewards'), $single),
				    'search_items' => sprintf(__('Search %s', 'lb-rewards'), $plural),
				    'not_found' =>  sprintf(__('There are not %s', 'lb-rewards'), $plural),
				    'not_found_in_trash' => sprintf(__('There are not %s in trash', 'lb-rewards'), $plural),
				    'parent_item_colon' => ''
				  );

				  $args = array(
						'labels' => $labels,
						'hierarchical'        => false,
						'public'              => true,
						'show_ui'             => true,
						'show_in_menu'        => true,
						'show_in_nav_menus'   => true,
						'show_in_admin_bar'   => true,
						'menu_position'       => $position,
						'menu_icon'	      		=> $menu_icon,
						'can_export'          => true,
						'has_archive'         => false,
						'exclude_from_search' => $exclude,
						'publicly_queryable'  => false,
						'capability_type'     => $capability_type,
						'taxonomies'					=> $taxonomies,
				    'supports' 						=> $support,
						'rewrite'		    			=> array('slug' => $rewrite)
				  );

				  register_post_type($the_type, $args);
			}

			}

add_filter('pre_get_posts', 'lb_rewards_query_cpt');

function lb_rewards_query_cpt($query) {
  if( is_category() ) {
    $post_type = get_query_var('post_type');
    if($post_type)
        $post_type = $post_type;
    else
        $post_type = array('reward');
    $query->set('post_type',$post_type);
    return $query;
    }
}
