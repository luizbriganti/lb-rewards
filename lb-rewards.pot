#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: LB Rewards for WooCommerce\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-11 10:48+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.2.1; wp-5.1"

#: linebreak_rewards_for_woocommerce.php:54 inc/lb_reward_request_form.php:6
#: inc/lb_rewards_cpt.php:12
#, php-format
msgid "%s Rewards"
msgstr ""

#: linebreak_rewards_for_woocommerce.php:77
msgid "User transactions history"
msgstr ""

#: inc/lb_reward_request_form.php:8
#, php-format
msgid ""
"Your balance is of <strong>%u points</strong>. You may ask to convert your "
"points into coupon."
msgstr ""

#: inc/lb_reward_request_form.php:10
msgid "You have no points yet"
msgstr ""

#: inc/lb_reward_request_form.php:16
#, php-format
msgid "You have %u unspent coupon:"
msgstr ""

#: inc/lb_reward_request_form.php:23
#, php-format
msgid "Value: %u&percnt; discount"
msgstr ""

#: inc/lb_reward_request_form.php:34
msgid "Submit a request for reward credit"
msgstr ""

#: inc/lb_reward_request_form.php:36
msgid ""
"Have you bought one of our products in a store offline? You may now collect "
"points from your shopping. Just upload a photo of your receipt and the "
"amount spent will be converted in points."
msgstr ""

#: inc/lb_reward_request_form.php:39
msgid "Upload a photo of your receipt"
msgstr ""

#: inc/lb_reward_request_form.php:43
msgid "Insert a note for your request."
msgstr ""

#: inc/lb_reward_request_form.php:53
msgid "Submit request"
msgstr ""

#: inc/lb_rewards_admin_columns.php:6
msgid "Request title"
msgstr ""

#: inc/lb_rewards_admin_columns.php:7
msgid "User requesting"
msgstr ""

#: inc/lb_rewards_admin_columns.php:8
msgid "Coupon Generated"
msgstr ""

#: inc/lb_rewards_admin_columns.php:9 inc/lb_user_history.php:32
msgid "Date"
msgstr ""

#: inc/lb_rewards_admin_columns.php:34 inc/lb_user_history.php:55
#: inc/lb_rewards_meta.php:202
msgid "Yes"
msgstr ""

#: inc/lb_rewards_admin_columns.php:35 inc/lb_user_history.php:55
#: inc/lb_rewards_meta.php:205
msgid "No"
msgstr ""

#: inc/lb_insert_reward_request.php:74
msgid "No images found"
msgstr ""

#: inc/lb_insert_reward_request.php:80
msgid "Your request has been sent correctly"
msgstr ""

#: inc/lb_rewards_cpt.php:11
#, php-format
msgid "%s Reward"
msgstr ""

#: inc/lb_rewards_cpt.php:39 inc/lb_rewards_cpt.php:40
msgid "Add new"
msgstr ""

#: inc/lb_rewards_cpt.php:41
#, php-format
msgid "Edit %s"
msgstr ""

#: inc/lb_rewards_cpt.php:42
#, php-format
msgid "New %s"
msgstr ""

#: inc/lb_rewards_cpt.php:43
#, php-format
msgid "View %s"
msgstr ""

#: inc/lb_rewards_cpt.php:44
#, php-format
msgid "Search %s"
msgstr ""

#: inc/lb_rewards_cpt.php:45
#, php-format
msgid "There are not %s"
msgstr ""

#: inc/lb_rewards_cpt.php:46
#, php-format
msgid "There are not %s in trash"
msgstr ""

#: inc/lb_user_history.php:5
msgid "User's transactions history"
msgstr ""

#: inc/lb_user_history.php:6
msgid "Here you can keep trace of the transactions for each user."
msgstr ""

#: inc/lb_user_history.php:8
msgid "Users"
msgstr ""

#: inc/lb_user_history.php:8
msgid "Total transactions"
msgstr ""

#: inc/lb_user_history.php:8
msgid "Details"
msgstr ""

#: inc/lb_user_history.php:24
msgid "View all"
msgstr ""

#: inc/lb_user_history.php:27
msgid "No transactions to show"
msgstr ""

#: inc/lb_user_history.php:32
msgid "Points gained/spent"
msgstr ""

#: inc/lb_user_history.php:32
msgid "Coupon generated"
msgstr ""

#: inc/lb_user_history.php:32
msgid "Notes"
msgstr ""

#: inc/lb_user_history.php:47
#, php-format
msgid ""
"This coupon was generated subtracting %1u points from user's balance and %2u "
"points from receipt uploaded, for a total of %3u&percnt; discount."
msgstr ""

#: inc/lb_user_history.php:62
msgid "User's current balance: "
msgstr ""

#: inc/lb_user_history.php:62
msgid "Transactions balance: "
msgstr ""

#: inc/lb_user_history.php:62
msgid "Coupons generated: "
msgstr ""

#: inc/lb_rewards_meta.php:10
msgid "User"
msgstr ""

#: inc/lb_rewards_meta.php:28
msgid "Points to credit/debit"
msgstr ""

#: inc/lb_rewards_meta.php:35
msgid ""
"If adding a negative value, points will be subtracted from user's balance."
msgstr ""

#: inc/lb_rewards_meta.php:46
msgid "A note from user"
msgstr ""

#: inc/lb_rewards_meta.php:64
msgid "Generate a coupon?"
msgstr ""

#: inc/lb_rewards_meta.php:71
#, php-format
msgid ""
"Insert a value to generate one coupon from the given import (10 points: 1% "
"discount)"
msgstr ""

#: inc/lb_rewards_meta.php:123
#, php-format
msgid "This user has a balance of %u points"
msgstr ""

#: inc/lb_rewards_meta.php:132
msgid ""
"If checked, you add to the coupon the amount subtracted from the user's "
"balance specified in the \"Point to credit/debit\" box."
msgstr ""

#: inc/lb_rewards_meta.php:271
msgid "Accept request"
msgstr ""

#: inc/lb_rewards_meta.php:274
msgid "Repeat transaction"
msgstr ""

#: inc/lb_rewards_user_fields.php:7
msgid "Rewards information"
msgstr ""

#: inc/lb_rewards_user_fields.php:11
msgid "User balance"
msgstr ""

#: inc/lb_rewards_user_fields.php:14
msgid "Update user balance here"
msgstr ""

#. Name of the plugin
msgid "LB Rewards for WooCommerce"
msgstr ""

#. Description of the plugin
msgid ""
"Manage rewards plans for users. With this plugin users may load a receipt "
"for their shopping offline and request a conversion in points. The shop "
"manager can generate a coupon from the points collected by the user."
msgstr ""

#. URI of the plugin
msgid "*"
msgstr ""

#. Author of the plugin
msgid "Luigi Briganti"
msgstr ""

#. Author URI of the plugin
msgid "http://www.linebreak.it"
msgstr ""
