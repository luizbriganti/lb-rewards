<?php

function lb_rewards_add_columns( $columns ) {
    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __('Request title','lb-rewards'),
        'user_requesting' => __('User requesting','lb-rewards'),
        'coupon_generated' => __('Coupon Generated','lb-rewards'),
        'date' => __('Date', 'lb-rewards')
    );
    return $columns;
}

function lb_rewards_add_columns_data( $column, $post_id ) {

    global $post;

    switch ( $column ) {
    case 'meta_author':
      if (get_post_meta($post_id, 'insta_author', true) ){
        echo get_post_meta( $post_id, 'insta_author', true );
      } else { echo ''; }
        break;
    case 'user_requesting':
      $rewUsr = get_post_meta($post_id, 'lb_reward_user', true);
      if($rewUsr){
        $user = get_user_by('ID', $rewUsr);
        echo $user->user_login;
      } else { echo ''; }
        break;
    case 'coupon_generated':
      $coupon = get_post_meta($post_id, 'lb_reward_coupon', true);
      if($coupon){
        echo __('Yes', 'lb-rewards');
      } else { echo __('No', 'lb-rewards'); }
        break;
    }
}

if ( function_exists( 'add_theme_support' ) ) {
    add_filter( 'manage_reward_posts_columns' , 'lb_rewards_add_columns' );
    add_action( 'manage_reward_posts_custom_column' , 'lb_rewards_add_columns_data', 10, 2 );
}
