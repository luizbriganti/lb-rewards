$ = jQuery;

$(document).ready(function(){
  $('.users .user').each(function(){
    $(this).children('.view').click(function(){
      console.log('clicked');
      $(this).siblings('.transactions-container').addClass('expanded').slideToggle();
      $(this).parent('li').siblings('.user').children('.transactions-container').removeClass('expanded').toggleUp();
    })
  })
});
